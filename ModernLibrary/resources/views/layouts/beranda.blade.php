@extends('layouts.template')

@section('title', 'Beranda')
@section('content')
    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-header">
                            Jumlah Buku
                        </div>
                        <div class="card-body">
                            <h5 class="card-title">{{ $jmlbook }}</h5>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="card">
                        <div class="card-header">Jumlah Kategory</div>
                        <div class="card-body">
                            <h5 class="card-title">{{ $jmlcat }}</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                @foreach ($book as $item)
                    <div class="card mb-3 mt-3 m-3" style="width: 18rem">
                        <img src="{{ asset('storage/' . $item->cover) }}" class="card-img-top" alt="Wild Landscape" />
                        <div class="card-body">
                            <h5 class="card-title">{{ $item->judul_buku }}</h5>
                            <p class="card-text">
                                Penerbit : {{ $item->penerbit }} <br>
                                Sinopsis : {{ $item->sinopsis }} <br>
                                Keterangan : {{ $item->keterangan }}
                            </p>
                            <p class="card-text">
                                <small class="text-muted">Kategori : {{ $item->category->nama_kategori }}</small>
                            </p>
                            
                            @if (DB::table('loans')
                                ->where('user_id',Auth::user()->id)
                                ->where('status', 1)
                                ->where('book_id', $item->id)
                                ->exists())

                            <form action="{{ url('updateloan') }}" method="POST">
                                @csrf
                                <input type="hidden" value="{{ $item->id }}" name="book_id">
                                <input type="hidden" value="{{ Auth::user()->id }}" name="user_id">

                                <button type="submit" class="btn btn-danger">Kembalikan Buku</button>
                            </form>
                            @else
                            <form action="{{ url('beranda') }}" method="POST"> 
                                @csrf 
                                <input type="hidden" value="{{ $item->id }}" name="book_id">
                                <input type="hidden" value="{{ Auth::user()->id }}" name="user_id">
                                    
                                <button type="submit" class="btn btn-primary">Pinjam Buku</button>
                            </form>
                            @endif
                        </div>
                    </div>
                @endforeach
                
            </div>
        </div>
            
    </div>
    @endsection
