@extends('layouts.template')

@section('content')
<div class="card mx-2 my-2">
    <h5 class="card-header">Kelola User</h5>
    <div class="table-responsive text-nowrap mx-2 my-1">
</div>
<div class="card-body mx-2">
    <h3 class="card">Profile Pengguna</h3>
    <h5>Nama Pengguna : <span>{{$user->name}}</span> </h5>
    <h5>Email : <span>{{$user->email}}</span> </h5>
    <h5>Role : <span>{{$user->role}}</span> </h5>
    <a href="/user" class="btn btn-secondary btn-sm">Kembali</a>

</div>



@endsection