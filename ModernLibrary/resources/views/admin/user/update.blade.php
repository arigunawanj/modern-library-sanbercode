@extends('layouts.template')

@section('content')
<div class="container-xxl flex-grow-1 container-p-y">
    <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">Data /</span> <span class="text-muted fw-light">Kelola Pengguna /</span> Tambah User</h4>
    <div class="col-xxl">
        <div class="card mb-4">
            <div class="card-header d-flex align-items-center justify-content-between">
                <h5 class="mb-0">Tambah Data User</h5>
            </div>
            <div class="card-body">
                <form action="/user/{{$user->id}}" method="POST">
                    @csrf
                    @method('put')
                    <div class="my-3">
                        <label for="">Nama Pengguna</label>
                        <input type="text" name="name" value="{{$user->name}}" class="form-control">
                        @error('name')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="my-3">
                        <label for="">Email Pengguna</label>
                        <input type="text" name="email" value="{{$user->email}}" class="form-control">
                        @error('email')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="my-3">
                        <label for="">Role</label>
                        <select name="role" id="" class="form-control">
                            <option value="admin" @if ('admin' == $user->role) @selected($user->role == 'admin') @endif >Admin</option>
                            <option value="customer" @if ('customer' == $user->role) @selected($user->role == 'customer') @endif>Customer</option>
                        </select>
                        @error('role')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>

                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>
</form>
    
@endsection