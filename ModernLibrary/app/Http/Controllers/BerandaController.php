<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\Loan;
use App\Models\Category;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class BerandaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $book = Book::where('tampil', 1)->get();
        $jmlbook = Book::count();
        $jmlcat = Category::count();
        $loan = Loan::all();
        return view('layouts.halamanutama',compact('book', 'jmlbook', 'jmlcat', 'loan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $book = Book::where('id', $request->book_id)->get();
        $book = Book::find($request->book_id);
        $data = Loan::create([
            'user_id' => $request->user_id,
            'book_id' => $request->book_id,
            'tanggal_peminjaman' => date('Y-m-d'),
            'jumlah_buku' => 1,
        ]);

        if($data){
            $book->update([
                'stock' => $book->stock - 1
            ]);
        }
        Alert::success('Berhasil Meminjam Buku');
        return redirect('beranda');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data = Loan::where('book_id', $request->book_id)->where('user_id', $request->user_id)->where('status', 1)->first();
        $book = Book::find($request->book_id);
        $data->update([
            'status' => 0,
        ]);

        if($data){
            $book->update([
                'stock' => $book->stock + 1,
            ]);
        }
        Alert::success('Berhasil Mengembalikan Buku');
        return redirect('beranda');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
