<?php

namespace App\Http\Controllers;

// use App\Models\Book;
use App\Models\Book;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use RealRashid\SweetAlert\Facades\Alert;


class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $book = Book::all();
        $category = Category::all();

        return view('admin.books.tampil' ,compact('book', 'category'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = Category::all();
        $book = Book::all();
        
        return view('admin.books.tambah',['category' => $category, 'book' =>$book] );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $validator = $request->validate([
            'isbn'=>'required',
            'judul_buku'=>'required',
            'penerbit'=>'required',
            'sinopsis'=>'required',
            'keterangan'=>'required',
            'cover' => 'required|image|max:10000|mimes:jpg',
            'stock'=>'required',
            'category_id' => 'required'

        ]);

        $newName = '';

        if($request->file('cover')){
            $extension = $request->file('cover')->getClientOriginalExtension();
            $newName = $request->judul.'-'.now()->timestamp.'.'.$extension;
            $data = $request->file('cover')->storeAs('img', $newName);
        };

        $validator['cover'] = $data;

        Book::create($validator);
        Alert::success('Berhasil Menambahkan Buku');
        return redirect('books');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function show(Book $book)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $book = Book::findOrFail($id);
        $category = Category::all();

        return view('admin.books.edit' ,compact('book', 'category'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $request->validate([
            'isbn' => 'required',
            'judul_buku' => 'required',
            'penerbit' => 'required',
            'sinopsis' => 'required',
            'keterangan' => 'required',
            'category_id' => 'required',
            'stock' => 'required'
        ]);
        
        $book = Book::findOrFail($id);
        
        $newName = '';
        $data = '';
        if($request->hasFile('cover')){
            $request->validate([
                'cover' => 'required|image|max:10000|mimes:jpg'
            ]);
            Storage::delete($book->cover);
            $cover = $request->cover;
            $extension = $request->file('cover')->getClientOriginalExtension();
            $newName = $request->isbn.'-'.now()->timestamp.'.'.$extension;
            $data = $request->file('cover')->storeAs('img', $newName);
            $validator['cover'] = $data;
            $book->update($validator);
            Alert::success('Berhasil Update Buku');
        } else {
            $book->update([
                'isbn' => $request->isbn,
                'judul_buku' => $request->judul_buku,
                'penerbit' => $request->penerbit,
                'sinopsis' => $request->sinopsis,
                'keterangan' => $request->keterangan,
                'category_id' => $request->category_id,
                'stock' => $request->stock,
                'cover' => $book->cover
            ]);
        }
        

       
        Alert::success('Berhasil Update Buku');        
        return redirect('books');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $book = Book::findOrFail($id);
        if(Storage::delete($book->cover)){
            Storage::delete($book->cover);
        }
        $book->delete();
        Alert::success('Berhasil Menghapus Buku');
        return redirect('books');
    }

    public function hide($id)
    {
        $book = Book::findorFail($id);
        if($book->tampil == 1){
            $book->update([
                'tampil' => 0
            ]);
        } else {
            $book->update([
                'tampil' => 1
            ]);
        }
        return redirect('books');
    }
}
