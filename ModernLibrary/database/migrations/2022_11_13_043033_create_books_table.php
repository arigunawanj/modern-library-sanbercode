<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->id();
            $table->string('isbn')->unique();
            $table->string('judul_buku');
            $table->string('penerbit');
            $table->string('sinopsis');
            $table->string('keterangan');
            $table->string('cover');
            $table->integer('stock');
            $table->boolean('tampil')->default(1);
            $table->foreignId('category_id')->constrained()->onCascadeUpdate()->onCascadeDelete();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
};
