<?php

namespace Database\Seeders;

use Carbon\Carbon;
use App\Models\Category;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        Category::truncate();
        Schema::enableForeignKeyConstraints();

        $data = [
            ['nama_kategori' => 'Agama'],
            ['nama_kategori' => 'Hukum'],
            ['nama_kategori' => 'Fisika'],
            ['nama_kategori' => 'Novel'],
            ['nama_kategori' => 'Teknologi'],
            ['nama_kategori' => 'Kesenian'],
            ['nama_kategori' => 'Literatur'],
            ['nama_kategori' => 'Sastra'],
            ['nama_kategori' => 'Geografi'],
        ];

        foreach ($data as $value) {
            Category::insert([
                    'nama_kategori' => $value['nama_kategori'],
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ]);
        }
    }
}
